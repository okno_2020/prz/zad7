using namespace std;
#include <iostream>
#include <ctime>
#include <iomanip>

const int w = 7;
const int k = 8;
const int r = 8;
const int g = 6;
const int d = 3;


float randomNumber(float min, float max) {
    static bool once = true;
    if (once) {
        srand(time(NULL));
        once = false;
    }
    return ((max - min) * ((float)rand() / RAND_MAX)) + min;
}

void fillArray(float Tab[w][k], int min, int max ) {
    for (int i = 0; i < w; ++i) {
        for (int j = 0; j <k; ++j) {
            Tab[i][j] = randomNumber(min, max);
        }
    }
}

void printArray(float Tab[w][k], int precision, int width) {
    for (int i = 0; i < w; ++i) {
        for (int j = 0; j <k; ++j) {
            cout << fixed << setw(width) << setprecision(precision) << Tab[i][j] << '\t';
        }
        cout << endl;
    }
    cout << endl;
}

void flipArray(float Tab[], int size) {
    float TempTab[size];
    for (int i = 0; i < size; ++i) {
        TempTab[size - 1 - i] = Tab[i];
    }
    for (int i = 0; i < size; ++i) {
        Tab[i] = TempTab[i];
    }
}

int flipArrayRowIfSumIsSmaller(float Tab[w][k], int number) {
    int flipped = 0;
    for (int i = 0; i < w; ++i) {
        float sum = 0;
        for (int j = 0; j < k; ++j) {
            sum += Tab[i][j];
        }
        if (sum < number) {
            flipArray(Tab[i], k);
            flipped++;
        }
    }
    return flipped;
}

int main() {
    float A[w][k];
    float B[w][k];
    int userTypedNumber;
    int flipped[2];

    fillArray(A, (r * -1), r);
    fillArray(B, d, g);

    cout<< "Tablica A \n";
    printArray(A, 1, 7);
    cout<< "Tablica B \n";
    printArray(B, 2, 7);

    cout<<"podaj liczbe: ";
    cin >> userTypedNumber;
    flipped[0] = flipArrayRowIfSumIsSmaller(A,0);
    flipped[1] = flipArrayRowIfSumIsSmaller(B, userTypedNumber);

    cout<< "Tablica A \n";
    printArray(A, 1, 7);
    cout<< "Tablica B \n";
    printArray(B, 2, 7);

    if(flipped[0] == flipped[1]){
        cout << "w obu tablicach odwrocono wiersz " << flipped[0] << " razy\n";
    } else {
        cout
            << "w tablicy "
            << ((flipped[0] > flipped[1]) ? 'A' : 'B')
            << " odwrocono wiersz wiecej razy (" << flipped[0] << ")\n";
    }
}

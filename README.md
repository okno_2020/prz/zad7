# Zadanie 7 – z funkcjami cz. 1 (0.75 pkt)

1. Tablicę A[w][k] (w, k - stałe) wypełnić liczbami losowymi rzeczywistymi z przedziału
<-r, r>, zaś tablicę B[w][k] - z przedziału <d, g> (r, d, g – stałe).
2. Tablicę A wydrukować wierszami z dokładnością 1 miejsca po kropce, tablicę B – z
dokładnością 2 miejsc.
3. W tablicy A odwrócić kolejność elementów (czyli zamienić pierwszy z ostatnim, drugi z
przedostatnim itd.) w wierszach o ujemnej sumie elementów, zaś w tablicy B zrobić to
samo w wierszach, w których suma elementów jest mniejsza od wczytanej wartości.
4. Ponownie wydrukować obie tablice.
5. Wydrukować informację, w której tablicy odwrócono więcej wierszy (a może tyle samo).

__W programie wykorzystać:__

- funkcję, która **jakąś** tablicę wypełnia liczbami losowymi rzeczywistymi zawartymi między
dwiema **jakimiś** wartościami.
- funkcję, która **jakąś** tablicę podanego typu drukuje wierszami z **jakąś** precyzją (liczbą
miejsc po kropce).
- funkcję, która dla **jakiejś** tablicy podanego typu odwraca kolejność elementów w tych
wierszach, w których suma elementów jest mniejsza od **jakiejś** wartości i **zwraca** liczbę
wierszy, w których dokonano takiego odwrócenia.

UWAGA: słowo **jakiś** oznacza parametr funkcji
